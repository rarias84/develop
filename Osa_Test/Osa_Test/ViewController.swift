//
//  ViewController.swift
//  Osa_Test
//
//  Created by Reinaldo Arias on 3/31/17.
//  Copyright © 2017 Reinaldo Arias. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    // IBOutlet
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var centerConstraint: NSLayoutConstraint!
    // Constants
    let cellIdentifier = "cell"
    // Variables
    var movies = [Movies]()
    var imgView = UIImageView()
    var v = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "graphy-osa")!)
        self.popupView.isHidden = true
        getData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        UIApplication.shared.statusBarStyle = .lightContent
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.orange]
    }
    
    // MARK: Function Helper
    func getData () {
        _ = Api().apiCall(completionHandler: { (response, error) in
            if response != nil {
                if let result = response?["Search"] as? [[String: Any]] {
                    for data in result{
                        let movieData : Movies = Movies(moviesDictionary: data as Dictionary<String, AnyObject>)
                        self.movies.append(movieData)
                    }
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                }
            } else {
                print("error - \(String(describing: error))")
            }
        })
    }
    
    // MARK: CollectionView Delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {        
        return movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as? CustomCollectionViewCell {
            let movie:Movies!
            movie = movies[indexPath.row]
            cell.configureCell(movie)
            return cell
        } else {
            return CustomCollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let poster = movies[indexPath.row]
        showPopup(poster.urlImage,title: poster.title)
    }
    
    func showPopup(_ path:String!, title:String!) {
        let posterURL = URL(string: path)
        let posterImageData = try? Data(contentsOf: posterURL!)
        
        centerConstraint.constant = 0
        self.imageView.image = UIImage(data: posterImageData!)
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0, options: .curveEaseOut, animations: {
            self.imageView.isHidden = false
            self.popupView.isHidden = false
            self.popupView.backgroundColor = UIColor(white: 1, alpha: 0.5)
            self.title = title
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        collectionView.reloadData()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            
            if (touch.view == imageView){
            } else {
                UIView.animate(withDuration: 0.1, animations: {
                    self.imageView.isHidden = true
                    self.popupView.isHidden = true
                    self.title = "Star Wars Movies"
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
}
