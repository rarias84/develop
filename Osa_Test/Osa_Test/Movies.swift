//
//  Movies.swift
//  Osa_Test
//
//  Created by Reinaldo Arias on 3/31/17.
//  Copyright © 2017 Reinaldo Arias. All rights reserved.
//

import UIKit

class Movies {
    var _title:String?
    var _urlImage:String?
    
    var title:String {
        if _title == nil {
            _title = ""
        }
        return _title!
    }
    var urlImage:String {
        if _urlImage == nil {
            _urlImage = "poster"
        }
        return _urlImage!
    }
    
    init(moviesDictionary: Dictionary<String, AnyObject>) {
        guard let title = moviesDictionary["Title"] as? String,
            let urlImage = moviesDictionary["Poster"] as? String else { return }
        self._title = title.capitalized
        self._urlImage = urlImage
    }
}
