//
//  CustomCollectionViewCell.swift
//  Osa_Test
//
//  Created by Reinaldo Arias on 3/31/17.
//  Copyright © 2017 Reinaldo Arias. All rights reserved.
//

import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var posterImgView: UIImageView!
    @IBOutlet weak var visualEffectView: UIVisualEffectView!
    @IBOutlet weak var titleLbl: UILabel!
    
    func configureCell(_ movieData:Movies) {
        titleLbl.text = movieData.title
        imageFromPath(movieData.urlImage)
    }
    
    // MARK: - Image Helper
    func imageFromPath(_ path: String) {
        let posterURL = URL(string: path)
        let posterImageData = try? Data(contentsOf: posterURL!)
        posterImgView.layer.cornerRadius = 100.0
        posterImgView.clipsToBounds = true
        posterImgView.image = UIImage(data: posterImageData!)
    }
}
