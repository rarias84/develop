//
//  Api.swift
//  Osa_Test
//
//  Created by Reinaldo Arias on 3/31/17.
//  Copyright © 2017 Reinaldo Arias. All rights reserved.
//

import Foundation

class Api {
    let url = "http://www.omdbapi.com/?s=Star+Wars&ref_=fn_ft"
    func apiCall(completionHandler: @escaping ([String:Any]?, Error?) -> Void ) -> URLSessionTask {
        let request = URLRequest(url: URL(string:url)!)
        let session = URLSession.shared
        let task = session.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print("Error calling - \(error!)")
                completionHandler(nil,error)
                return
            }
            guard let responseData = data else {
                print("Error: did not receive data")
                completionHandler(nil,error)
                return
            }
            do {
                guard let todo = try JSONSerialization.jsonObject(with: responseData, options: .allowFragments)
                    as? [String: Any] else {
                        print("Error trying to convert data to JSON")
                        return
                }
                completionHandler(todo, nil)
                return
            } catch let error {
                print("Error trying to convert data to JSON - \(error)")
                completionHandler(nil,error)
                return
            }
        }
        task.resume()
        return task
    }
}
